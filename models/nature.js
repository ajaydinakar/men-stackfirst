var mongoose =require("mongoose");
//create a schema (structure of database elements)

var imgSchema = new mongoose.Schema({
   name: String,
   url: String,
   about: String,
   comments: [
      {
         type: mongoose.Schema.Types.ObjectId,
         ref: "Comment"
      }
   ]
});
    
    //create a model and export


module.exports=mongoose.model("image",imgSchema);
