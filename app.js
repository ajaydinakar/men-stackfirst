var express=require("express");
var app=express();
app.use(express.static("public"));

var bodyParser=require("body-parser");//this is used for retriving data from form data during post route
var mongoose =require("mongoose");
var ObjectId = require('mongodb').ObjectID;
var override=require("method-override");
var image=require("./models/nature");
var Comment   = require("./models/comment");
seedDB=require("./seeds");
app.set("view engine","ejs");

//seedDB();

//connect to database named natpic

mongoose.connect("mongodb://localhost/natpic");
app.use(bodyParser.urlencoded({extended:true}));
app.use(override("_method"));




    
   
app.get("/",function(req,res){res.render("home");});

app.get("/new",function(req,res){res.render("new");});



//render route to diplay html page which is in views directory ,

//******************************************************************************
app.get("/dashboard",function(req,res){
    image.find({},function(error,allpictures)
    {
        if(error) 
        {
      console.log(error);
        }
      else
      {
          console.log("retrived");
       res.render("dashboard",{imgs:allpictures});
      } 
    });
    
   });


//**************************************************************
//
//****************************************************************************************
app.post("/dashboard",function(req,res)
{
var newname=req.body.name;
var newimage=req.body.image;
var newabout=req.body.about;
var newimgs={name:newname,url:newimage,about:newabout};
image.create(newimgs,function(error,image)
    {
      if(error)
      console.log("error");
      else
      {
          console.log("success");
     res.redirect("/dashboard");
      }
    });

});

//**************************************************************
//
//***************************************************************************

app.get("/dash/:id",function(req,res)
{

console.log(req.params.id);
var id= ObjectId(req.params.id);
console.log(id); 
image.findById(id,function(error,info)
{
 
   
 if(error) 
 {
     console.log("error");
 }
 else { 
    //console.log(data);
     res.render("show",{info:info}) ;   
       }
}
);
});


//**************************************************************
//
//*******************************************************************************







//**************************************************************
//
//*******************************************************************

app.put("/dash/:id",function (req,res)

  {
var id= ObjectId(req.params.id);
var newname=req.body.name;
var newimage=req.body.image;
var newabout=req.body.about;
var newimgs={name:newname,url:newimage,about:newabout};
image.findByIdAndUpdate(id,newimgs,function(error,info)
{
 
   
 if(error) 
 {
     res.render("/dashboard");
 }
 else { 
    //console.log(data);
     res.redirect("/dash/"+id) ;   
       }
}
);

  
  });


//**************************************************************
//
//**************************************************************


app.get("/dash/:id/edit",function (req,res)
  {

var id= ObjectId(req.params.id).toString('hex');

image.findById(id,function(error,info)
{
 
   
 if(error) 
 {
     res.render("/dashboard");
 }
 else { 
    //console.log(data);
     res.render("edit",{info:info}) ;   
       }
}
);

  });



//**************************************************************
//               Default route
//**************************************************************
app.delete("/dash/:id",function(req,res)
  {
    var id= ObjectId(req.params.id).toString('hex');
    image.findByIdAndRemove(id,function(error,info)
    {
      if(error) 
 {
     res.render("show");
 }
 else { 
    //console.log(data);
     res.redirect("/dashboard") ;   
       }
});
    }


  );






//**************************************************************






//**************************************************************
//               Default route
//**************************************************************
app.get("*",function(req,res){res.redirect("/");});
//**************************************************************




//Make express to listen for the requests 
app.listen(3000,function(){console.log("Server Working!");});
